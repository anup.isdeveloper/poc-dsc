package com.dell.dsc.pocdsc2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Pocdsc2Application

fun main(args: Array<String>) {
	runApplication<Pocdsc2Application>(*args)
}
